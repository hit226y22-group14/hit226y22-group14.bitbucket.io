//***Initial Attempt couldn't cycle images***//
// function myFunction() {
//   document.getElementById("img").innerHTML = "<img class='pic' src='nrl-ball.jpg' alt='NRL Ball'>";
// }


//***Javascript now able to Cycle images with IF statement and tracking of the Images status***//

// Cycle through images on click//
// https://www.youtube.com/watch?v=SGKXZUGe2sw //

var imageStatus = "logo";

function changeImage() {
    var image = document.getElementById("pic");
    var stext = document.getElementById("stext");
    stext.innerHTML = "<div class='stext' id='stext'>Catch up on all the latest news and stats of the NRL or select your team for all their latest updates. <br><strong>LIVE</strong> and <strong>FREE</strong> all the content you could want!</div>";
    if (imageStatus == "logo"){
        image.src = "nrl-ball.jpg";
        imageStatus = "ball";
    }else{
        image.src = "nrl.png";
        imageStatus = "logo";
    }
}

function displayNews() {
    var news = document.getElementById("stext");
    var image = document.getElementById("pic");
    news.innerText = "Roosters confirmed as the BEST team in the NRL. The team has beaten out every other team in the competiton by a landslide. NSW blues tipped to demolish QLD maroons in the upcoming State of Origin.";
    if (imageStatus !== "roosters"){
        image.src = "roosters.png";
        imageStatus = "roosters";
    }
}

function displayTeams() {
    var teams = document.getElementById("stext");
    var image = document.getElementById("pic");
    teams.innerHTML = "<ul><li><a href=''>Brisbane Broncos</a> <li><a href=''>Canberra Raiders</a> <li><a href=''>Canterbury Bulldogs</a> <li><a href=''>Cronulla Sharks</a> <li><a href=''>Gold Coast Titans</a> <li><a href=''>Manly Sea Eagles</a> <li><a href=''>Melbourne Storm</a> <li><a href=''>New Zealand Warriors</a> <li><a href=''>Newcastle Knights</a> <li><a href=''>North Queensland Cowboys</a> <li><a href=''>Parramatta Eels</a> <li><a href=''>Penrith Panthers</a> <li><a href=''>South Sydney Rabbitohs</a> <li><a href=''>St George Illawarra Dragons</a> <li><a href=''>Sydney Roosters</a> <li><a href=''>Wests Tigers</a></ul>";
    if (imageStatus !== "teams"){
        image.src = "teams.jpeg";
        imageStatus = "teams";
    }
}
