// variables
const generalBtn = document.getElementById("general");
const soccerBtn = document.getElementById("soccer");
const cricketBtn = document.getElementById("cricket");
const rugbyBtn = document.getElementById("rugby");
const basketballBtn = document.getElementById("basketball");
const searchBtn = document.getElementById("searchBtn");

const newsQuery = document.getElementById("newsQuery");
const newsType = document.getElementById("newsType");
const newsdetails = document.getElementById("newsdetails");
const signupBtn = document.getElementById("signup");

signupBtn.addEventListener('click', (e)=>{
    window.open("Sandeep/form/index.html", "_self");
    }
)

window.addEventListener('scroll', reveal);

function reveal(){
  var reveals = document.querySelectorAll('.reveal');

  for(var i = 0; i < reveals.length; i++){

    var windowheight = window.innerHeight;
    var revealtop = reveals[i].getBoundingClientRect().top;
    var revealpoint = 150;

    if(revealtop < windowheight - revealpoint){
      reveals[i].classList.add('active');
    }
    else{
      reveals[i].classList.remove('active');
    }
  }
}

