// All cards //
// NRL Card //

var imageStatusNrl = "nrl-logo";

function changeImageNrl() {
    var image = document.getElementById("nrl");
    var stext = document.getElementById("stext-nrl");
    stext.innerHTML = "<div class='stext-nrl' id='stext'>Catch up on all the latest news and stats of the NRL or select your team for all their latest updates. <br><strong>LIVE</strong> and <strong>FREE</strong> all the content you could want!</div>";
    if (imageStatusNrl == "nrl-logo"){
        image.src = "../Images/card-nrl/nrl-ball.jpg";
        imageStatus = "nrl-ball";
    }else{
        image.src = "../Images/card-nrl/nrl.png";
        imageStatusNrl = "nrl-logo";
    }
}

function displayNewsNrl() {
    var news = document.getElementById("stext-nrl");
    var image = document.getElementById("nrl");
    news.innerText = "Roosters confirmed as the BEST team in the NRL. The team has beaten out every other team in the competiton by a landslide. NSW blues tipped to demolish QLD maroons in the upcoming State of Origin.";
    if (imageStatusNrl !== "nrl-news"){
        image.src = "../Images/card-nrl/roosters.png";
        imageStatusNrl = "nrl-news";
    }
}

function displayTeamsNrl() {
    var teams = document.getElementById("stext-nrl");
    var image = document.getElementById("nrl");
    teams.innerHTML = "<ul><li><a href=''>Brisbane Broncos</a> <li><a href=''>Canberra Raiders</a> <li><a href=''>Canterbury Bulldogs</a> <li><a href=''>Cronulla Sharks</a> <li><a href=''>Gold Coast Titans</a> <li><a href=''>Manly Sea Eagles</a> <li><a href=''>Melbourne Storm</a> <li><a href=''>New Zealand Warriors</a> <li><a href=''>Newcastle Knights</a> <li><a href=''>North Queensland Cowboys</a> <li><a href=''>Parramatta Eels</a> <li><a href=''>Penrith Panthers</a> <li><a href=''>South Sydney Rabbitohs</a> <li><a href=''>St George Illawarra Dragons</a> <li><a href=''>Sydney Roosters</a> <li><a href=''>Wests Tigers</a></ul>";
    if (imageStatusNrl !== "nrl-teams"){
        image.src = "../Images/card-nrl/teams.jpeg";
        imageStatusNrl = "nrl-teams";
    }
}

function displayShopNrl() {
    var shop = document.getElementById("stext-nrl");
    var image = document.getElementById("nrl");
    shop.innerHTML = "<div class='stext' id='stext'>Get all the LATEST and GREATEST NRL merchandise here NOW!</div> <ul><li>Jerseys <li>Balls <li>Flags <li>Miscellanious</ul> <div><a href='https://www.nrlshop.com/' target = '_blank'>Start Shopping!</a><div> <br>";
    if (imageStatusNrl !== "nrl-shop"){
        image.src = "../Images/card-nrl/nrl-jersey.jpg";
        imageStatusNrl = "nrl-shop";
    }
}


// AFL CARD //
var imageStatusAfl = "afl-logo";

function changeImageAfl() {
    var image = document.getElementById("afl");
    var stext = document.getElementById("stext-afl");
    stext.innerHTML = "<div class='stext' id='stext'>Catch up on all the latest news and stats of the AFL or select your team for all their latest updates. <br><strong>LIVE</strong> and <strong>FREE</strong> all the content you could want!</div>";
    if (imageStatusAfl == "afl-logo"){
        image.src = "../Images/card-afl/afl-ball.png";
        imageStatus = "afl-ball";
    }else{
        image.src = "../Images/card-afl/afl.png";
        imageStatusAfl = "afl-logo";
    }
}

function displayNewsAfl() {
    var news = document.getElementById("stext-afl");
    var image = document.getElementById("afl");
    news.innerText = "Ruckman Hickey hasn't featured since hurting a knee in round three but the Swans are bullish about his availability for Saturday's game against Gold Coast. McCartin and McDonald are both tipped to return after missing last weekend's loss to Brisbane because of concussion protocols.";
    if (imageStatusAfl !== "afl-news"){
        image.src = "../Images/card-afl/swans.png";
        imageStatusAfl = "afl-news";
    }
}

function displayTeamsAfl() {
    var teams = document.getElementById("stext-afl");
    var image = document.getElementById("afl");
    teams.innerHTML = "<ul><li><a href=''>Adelaide Crows</a> <li><a href=''>Brisbane Lions</a> <li><a href=''>Carlton Blues</a> <li><a href=''>Collingwood Magpies</a> <li><a href=''>Essendon Bombers</a> <li><a href=''>Fremantle Dockers</a> <li><a href=''>Geelong Cats</a> <li><a href=''>Gold Coast Suns</a> <li><a href=''>Western Sydney Giants</a> <li><a href=''>Hawthorn Hawks</a> <li><a href=''>Melbourne Demons</a> <li><a href=''>North Melbourne Kangaroos</a> <li><a href=''>Port Adelaide Power</a> <li><a href=''>Richmond Tigers</a> <li><a href=''>St Kilda Saints</a> <li><a href=''>Sydney Swans</a> <li><a href=''>West Coast Eagles</a> <li><a href=''>Western Bulldogs</a></ul>";
    if (imageStatusAfl !== "afl-teams"){
        image.src = "../Images/card-afl/afl-teams.jpg";
        imageStatusAfl = "afl-teams";
    }
}

function displayShopAfl() {
    var shop = document.getElementById("stext-afl");
    var image = document.getElementById("afl");
    shop.innerHTML = "<div class='stext' id='stext'>Get all the LATEST and GREATEST AFL merchandise here NOW!</div> <ul><li>Jerseys <li>Balls <li>Flags <li>Miscellanious</ul> <div><a href='https://www.aflfootyshop.com.au/' target = '_blank'>Start Shopping!</a><div> <br>";
    if (imageStatusAfl !== "afl-shop"){
        image.src = "../Images/card-afl/afl-jersey.jpg";
        imageStatusAfl = "afl-shop";
    }
}

// EPL CARD //
var imageStatusAfl = "epl-logo";

function changeImageEpl() {
    var image = document.getElementById("epl");
    var stext = document.getElementById("stext-epl");
    stext.innerHTML = "<div class='stext' id='stext'>Catch up on all the latest news and stats of the English Premier League or select your team for all their latest updates. <br><strong>LIVE</strong> and <strong>FREE</strong> all the content you could want!</div>";
    if (imageStatusAfl == "epl-logo"){
        image.src = "../Images/card-epl/epl-ball.png";
        imageStatus = "epl-ball";
    }else{
        image.src = "../Images/card-epl/epl.png";
        imageStatusAfl = "epl-logo";
    }
}

function displayNewsEpl() {
    var news = document.getElementById("stext-epl");
    var image = document.getElementById("epl");
    news.innerText = "Stalling Salah has Euro giants ready to pounce; $124m star top of Gunners wish list: Rumour Mill. Mohamed Salah has enjoyed the best football of his career since moving to Liverpool from Roma in 2017, but could his time on Merseyside be coming to an end?";
    if (imageStatusAfl !== "epl-news"){
        image.src = "../Images/card-epl/gunners.png";
        imageStatusAfl = "epl-news";
    }
}

function displayTeamsEpl() {
    var teams = document.getElementById("stext-epl");
    var image = document.getElementById("epl");
    teams.innerHTML = "<ul><li><a href=''>Arsenal</a> <li><a href=''>Aston Villa</a> <li><a href=''>Brentford</a> <li><a href=''>Brighton & Hove Albion</a> <li><a href=''>Burnley</a> <li><a href=''>Chelsea</a> <li><a href=''>Crystal Palace</a> <li><a href=''>Everton</a> <li><a href=''>Leeds United</a> <li><a href=''>Leicester City</a> <li><a href=''>Liverpool</a> <li><a href=''>Manchester City</a> <li><a href=''>Manchester United</a> <li><a href=''>Newcastle United</a> <li><a href=''>Norwich City</a> <li><a href=''>Southampton</a> <li><a href=''>Tottenham Hotspur</a> <li><a href=''>Watford</a> <li><a href=''>West Ham United</a> <li><a href=''>Wolverhampton</a></ul>";
    if (imageStatusAfl !== "epl-teams"){
        image.src = "../Images/card-epl/epl-teams.jfif";
        imageStatusAfl = "epl-teams";
    }
}
// shop//

function displayShopEpl() {
    var shop = document.getElementById("stext-epl");
    var image = document.getElementById("epl");
    shop.innerHTML = "<div class='stext' id='stext'>Get all the LATEST and GREATEST EPL merchandise here NOW!</div> <ul><li>Jerseys <li>Balls <li>Flags <li>Miscellanious</ul> <div><a href='https://www.rebelsport.com.au/fangear/epl' target = '_blank'>Start Shopping!</a><div> <br>";
    if (imageStatusAfl !== "epl-shop"){
        image.src = "../Images/card-epl/epl-jersey.png";
        imageStatusAfl = "epl-shop";
    }
}

// var resultStatus = "off";

function displayResults() {
    var result = document.getElementById("result");
    result.innerHTML = "<p class='results'>Roosters vs Titans <br> 44 - 16</p> <a href='https://www.youtube.com/watch?v=Ryr-bWQdbK8' target = '_blank' <button type='button'>HIGHLIGHTS</button></a>";
}
