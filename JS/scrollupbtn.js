const scrollupbtn = document.querySelector("#scrollupbtn");
const refreshButton = () => {
    if (document.documentElement.scrollTop <= 300) {
        scrollupbtn.style.display = "none";
      } else {
        scrollupbtn.style.display = "block";
      }
};

refreshButton();

scrollupbtn.addEventListener("click", function (){
    window.scrollTo(0,0)  
});

document.addEventListener("scroll", (e) => {
    refreshButton();
});